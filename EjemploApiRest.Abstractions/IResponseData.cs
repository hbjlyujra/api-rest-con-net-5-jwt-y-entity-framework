﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploApiRest.Abstractions
{
    public interface IResponseData
    {
        bool FromCache { get; set; }
    }
}
