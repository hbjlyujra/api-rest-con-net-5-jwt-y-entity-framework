﻿using EjemploApiRest.Abstractions;
using EjemploApiRest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploApiRest.DataAccess
{
    public interface IFootballTeamDbContext : IDbContext<FootballTeam>
    {
        FootballTeam GetFootballTeamByManager(string manager);
    }
    public class FootballTeamDbContext : DbContext<FootballTeam>, IFootballTeamDbContext
    {
        public FootballTeamDbContext(ApiDbContext ctx): base(ctx)
        {
            
        }

        public FootballTeam GetFootballTeamByManager(string manager)
        {
            return GetAll().Where(t => t.Manager.Equals(manager)).FirstOrDefault();
        }
    }
}
