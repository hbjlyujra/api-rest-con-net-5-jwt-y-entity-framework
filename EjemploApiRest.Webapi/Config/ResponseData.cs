﻿using EjemploApiRest.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EjemploApiRest.Webapi.Config
{
   
    public class ResponseData : IResponseData
    {
        public bool FromCache { get; set; }
    }
}
